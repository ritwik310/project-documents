A very simple HTML-Parser with a linear runtime complexity O(n). (Not a package)

View Source-code - [https://github.com/ritwik310/html-parser](https://github.com/ritwik310/html-parser)

![GitHub](https://img.shields.io/github/license/ritwik310/html-parser.svg)
![Travis (.com)](https://img.shields.io/travis/com/ritwik310/html-parser.svg)

# Example

```python
from parser import parser # import from the parser (somehow :p)

f = open("index.html","rb")
d = f.read()
f.close()
ds = d.decode("utf-8")

document = parser.parse_str(ds) # return type explained below
```

# Document Tree

### Node

The `parser.parse_str` function retuns a document, which is a tree of nodes. Each node looks something like this,

<img src="https://gitlab.com/ritwik310/project-documents/raw/master/HTML-Parser/HTML-Parser-Node-Diagram-0.png" alt="Node Diagram"/>

### Document

The document that is returnd from the `parse_str` is root node of the tree. For example parsing the html code below will return a document tree like shown in the diagram,

```html
<html>
   <head>
      <title>Document</title>
   </head>
   <body>
      <h1>Header Here</h1>
      <p class="my-genuine-class">Some <b>bold</b> desciption</p>
   </body>
</html>
```

<img src="https://gitlab.com/ritwik310/project-documents/raw/master/HTML-Parser/HTML-Parser-Document-Tree-Diagram-1.png" alt="Document Tree Diagram"/>